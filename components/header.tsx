import { signOut } from "next-auth/react";
import { getServerSession } from "next-auth/next";

import Link from "next/link";


export default async function Header() {
  const session = await getServerSession();
  return (
    <div className="flex w-full justify-between py-4 px-8">
      <Link href="/" className="text-slate-200 text-xl font-bold font-display">Tracker</Link>
      {session ? (
        <p className="text-stone-200 text-sm">
          Signed in as {session.user?.email}
        </p>
      ) : <Link href="/login" className="text-slate-200 hover:underline">Login</Link>}
      
    </div>
  );
}