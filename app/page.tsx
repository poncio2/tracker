import Image from "next/image";
import Link from "next/link";
import Header from "@/components/header";

export default function Home() {
  return (
    <div className="flex h-screen bg-black">
      <h1>Home</h1>
    </div>
  );
}
