import Image from "next/image";
import Link from "next/link";
import Header from "@/components/header";

export default function Author() {
  return (
    <div className="flex h-screen bg-black">
      <h1 className="text-xl text-slate-200">Authors</h1>
    </div>
  );
}
