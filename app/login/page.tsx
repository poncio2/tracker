import Form from "@/components/form";
import Link from "next/link";

export default function Login() {
  return (
    <div className="flex h-screen w-screen mt-[-60px] items-center justify-center bg-black">
      <div className="z-10 w-full max-w-md overflow-hidden rounded-2xl border border-gray-900 shadow-xl">
        <div className="bg-black flex flex-col items-center justify-center space-y-3 border-b border-gray-800 px-4 py-6 pt-8 text-center sm:px-16">
          <Link href="/">

          </Link>
          <h3 className="text-xl font-semibold text-slate-200">Log In</h3>
          <p className="text-sm text-slate-200">
            Use your email and password to sign in
          </p>
        </div>
        <Form type="login" />
      </div>
    </div>
  );
}
