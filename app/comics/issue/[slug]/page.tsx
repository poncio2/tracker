import Image from "next/image";
import Link from "next/link";
import Header from "@/components/header";

export default function IssueWithSlug({ params }: { params: { slug: string } }) {
  return (
    <div className="flex h-screen bg-black">
      <h1 className="text-xl text-slate-200">Issue 1</h1>
    </div>
  );
}
